require 'PHPMailer/PHPMailerAutoload.php';
$mail = new PHPMailer;

// SMTP configuration
$mail->isSMTP();
$mail->Host = 'smtp.example.com';
$mail->SMTPAuth = true;
$mail->Username = 'user@example.com';
$mail->Password = '******';
$mail->SMTPSecure = 'tls';
$mail->Port = 587;

$mail->setFrom('info@example.com', 'CodexWorld');
$mail->addReplyTo('info@example.com', 'CodexWorld');

// Add a recipient
$mail->addAddress('john@gmail.com');

// Add cc or bcc 
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');

// Email subject
$mail->Subject = 'Send Email via SMTP using PHPMailer';

// Set email format to HTML
$mail->isHTML(true);

// Email body content
$mailContent = "<h1>Send HTML Email using SMTP in PHP</h1>
    <p>This is a test email has sent using SMTP mail server with PHPMailer.</p>";
$mail->Body = $mailContent;

// Send email
if(!$mail->send()){
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}else{
    echo 'Message has been sent';
}

Note that: You don’t need to download PHPMailer library files separately. Our source code contains all the required PHPMailer library files including the example script.
Send HTML Email with Attachments

Use addAttachment() method of PHPMailer class to add an attachment to the email. You can add multiple attachments to the email by adding addAttachment() method multiple times.

// Add attachments
$mail->addAttachment('docs/codexworld_1.pdf');
$mail->addAttachment('docs/codexworld_2.docs');
$mail->addAttachment('images/codexworld_3.png', 'new-name.png'); //set new name

Send Email to Multiple Recipients

Add addAddress() method multiple times for sending same email to the multiple recipients.

// Add multiple recipients
$mail->addAddress('john@gmail.com');
$mail->addAddress('doe@gmail.com');