jQuery(document).ready(function(){
	
	jQuery("#ejecutar").click(function(){		
		//cogemos el valor del input
		var num = jQuery("#numero").val();
		
		if( num == "" ){
			alert("Seleccion un rubro :)");
			return;
		}
		
		//creamos array de parámetros que mandaremos por POST
		var params = {
			"pasos" : num
		};
				
		//llamada al fichero PHP con AJAX
		$.ajax({
			data:  params,
			url:   'instalar.php',
			dataType: 'html',
			type:  'post',
			beforeSend: function () {
				//mostramos gif "cargando"
				jQuery('#loading_spinner').show();
				//antes de enviar la petición al fichero PHP, mostramos mensaje
				jQuery("#resultado").html("Instalando...");
			},
			success:  function (response) {
				//escondemos gif
				jQuery('#loading_spinner').hide();
				//mostramos salida del PHP
				jQuery("#resultado").html(response);
				
			}
		});
	
		
	});
		
	
});